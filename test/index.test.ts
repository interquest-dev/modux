import Modux, { IModule } from "../src/index";

type IngredientsState = string[];

type IngredientAction =
  | {
      ingredient: string;
      type: "ADD_INGREDIENT";
    }
  | {
      type: "CLEAR_INGREDIENTS";
    };

interface IIngredientsState {
  ingredients: IngredientsState;
  firstIngredient: string | null;
  additionalNotes: string[];
}

const module: IModule<IIngredientsState> = {
  name: "ingredientModule",
  initialState: { ingredients: [], firstIngredient: null, additionalNotes: [] },
  reducers: {
    firstIngredient: {
      dependencies: ["ingredients"],
      fn(
        state: string | null = null,
        action: IngredientAction,
        oldState: IIngredientsState,
        newState: IIngredientsState
      ) {
        if (
          action.type === "ADD_INGREDIENT" &&
          newState.ingredients.length > 0
        ) {
          return newState.ingredients[0];
        } else if (newState.ingredients.length === 0) {
          return null;
        }
        return state;
      }
    },
    ingredients: {
      fn(state: IngredientsState = [], action: IngredientAction) {
        if (action.type === "ADD_INGREDIENT") {
          return [...state, action.ingredient];
        }
        if (action.type === "CLEAR_INGREDIENTS") {
          return [];
        }
        return state;
      }
    }
  },
  loadReducers: async () => ({
    additionalNotes: {
      fn(state: string[] = [], action: IngredientAction) {
        if (action.type === "ADD_INGREDIENT") {
          return [...state, `${action.ingredient} added`];
        }
        return state;
      }
    }
  })
};

type RecipeAction = {
  name: string;
  type: "ADD_RECIPE";
};

interface IRecipesState {
  recipes: string[];
}

const secondModule: IModule<IRecipesState> = {
  initialState: { recipes: [] },
  name: "recipeModule",
  reducers: {
    recipes: {
      fn(state: string[] = [], action: RecipeAction) {
        if (action.type === "ADD_RECIPE") {
          return [...state, action.name];
        }
        return state;
      }
    }
  }
};

// The app state must always contain everything that can be in it,
// which includes the whole app.
type AppState = IIngredientsState & IRecipesState;

const initialState = {
  ...module.initialState,
  ...secondModule.initialState
};

test("Basic reducers", async () => {
  const modux = new Modux<AppState>([module], initialState);
  const store = await modux.createStore();
  store.dispatch({
    ingredient: "flour",
    type: "ADD_INGREDIENT"
  });
  let state = store.getState();
  expect(state.ingredients).toEqual(["flour"]);
  expect(state.firstIngredient).toEqual("flour");
  expect(state.additionalNotes.length).toEqual(1);
  store.dispatch({
    type: "CLEAR_INGREDIENTS"
  });

  state = store.getState();
  expect(state.ingredients).toEqual([]);
  expect(state.firstIngredient).toEqual(null);
});

test("After hooks", async () => {
  let afterRun = false;

  module.after = {
    ADD_INGREDIENT: [
      () => {
        afterRun = true;
      }
    ]
  };

  const modux = new Modux<AppState>([module], initialState);
  const store = await modux.createStore();

  store.dispatch({
    ingredient: "flour",
    type: "ADD_INGREDIENT"
  });

  expect(afterRun).toBe(true);
});

test("Onmoduleload on init", async () => {
  let onModuleLoadCalled = false;
  module.onModuleLoad = name => {
    if (name === "ingredientModule") {
      onModuleLoadCalled = true;
    }
  };
  const modux = new Modux<AppState>([module], initialState);
  await modux.createStore();
  expect(onModuleLoadCalled).toBe(true);
});

test("Init functions", async () => {
  let initRun = false;
  module.init = () => (initRun = true);
  const modux = new Modux<AppState>([module], initialState);
  await modux.createStore();
  expect(initRun).toBe(true);
});

test("Dynamicly add modules", async () => {
  let recipeLoaded = false;
  module.onModuleLoad = function(module: string) {
    if (module === "recipeModule") {
      recipeLoaded = true;
    }
  };
  const modux = new Modux<AppState>([module], initialState);
  const store = await modux.createStore();
  await modux.addModule(secondModule);
  store.dispatch({
    name: "First recipe",
    type: "ADD_RECIPE"
  });
  expect(recipeLoaded).toEqual(true);
  expect(store.getState().recipes).toContain("First recipe");
  // Adding the same module again should work as long as we defined a name for it.
  await modux.addModule(secondModule);
  expect(modux.getModules()).toHaveLength(2);
  store.dispatch({
    name: "Second recipe",
    type: "ADD_RECIPE"
  });
  expect(store.getState().recipes).toHaveLength(2);
});
