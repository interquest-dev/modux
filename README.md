# Modux

Modux is an attempt at a modular approach to redux. It lets you define
your reducers into a module that can then be added to a redux store
easily. It also provides a way for reducers to depend on other
reducers, so that the updated data is available to a reducer which
depends on another one.

# Defining a modux module

A module is a plan object of the interface IModule, the IModule
interface let's you pass in the State interface you want to use as a
generic:

```typescript
type Action =
  | {
      type: "INCREMENT";
    }
  | {
      type: "DECREMENT";
    }
  | {
      type: "SET";
      value: number;
    };
interface CounterState {
  counter: number;
}
const initialState = {
  counter: 0,
};

export const module: IModule<CounterState> = {
  initialState,
  reducers: {
    counter: {
      fn(state: CounterState = initialState, action: Action) {
        switch (action.type) {
          case "INCREMENT":
            return state.counter + 1;
          case "DECREMENT":
            return state.counter - 1;
          case "SET":
            return action.value;
        }
        return state;
      },
    },
  },
  name: "counter",
};

```

the module can then be added to a modux store like this:

```
const modux = new Modux<CounterState>([module], initialState);
const store = await modux.createStore()
```

The store variable is now a normal Redux store and can be used
normally.

## Combining modules into a combined store

The description above describes the simplest store possible, with only
one module. This is of course not very useful. Modux is built to be
able to encapsulate multiple modules that can be isolated from each
other:

```typescript
type CounterAction =
  | {
      type: "INCREMENT";
    }
  | {
      type: "DECREMENT";
    }
  | {
      type: "SET";
      value: number;
    };
interface CounterState {
  counter: number;
}
const counterInitialState = {
  counter: 0,
};

const counterModule: IModule<CounterState> = {
  initialState: counterInitialState,
  reducers: {
    counter: {
      fn(state: CounterState = initialState, action: CounterAction) {
        switch (action.type) {
          case "INCREMENT":
            return state.counter + 1;
          case "DECREMENT":
            return state.counter - 1;
          case "SET":
            return action.value;
        }
        return state;
      },
    },
  },
  name: "counter",
};

type QuoteAction =
  | {
      type: "CHANGE_QUOTE";
      quote: string;
    }
  | {
      type: "DELETE_QUOTE";
    };

interface QuoteState {
  quote: string | null;
}

const quoteInitialState = {
  quote: null,
};

const quoteModule: IModule<QuoteState> = {
  initialState: quoteInitialState,
  reducers: {
    quote: {
      fn(state: QuoteState, action: QuoteAction) {
        switch (action.type) {
          case "CHANGE_QUOTE":
            return action.quote;
          case "DELETE_QUOTE":
            return null;
        }
        return state;
      },
    },
  },
};

const modux = new Modux<CounterState & QuoteState>(
  [counterModule, quoteModule],
  { ...counterInitialState, ...quoteInitialState }
);
const store = await modux.createStore();

```

We now have two modules that composes a shared store with both
module reducers.

## Reducer dependencies

Reducers can depend on that another reducer has been run first. This is useful when
you have data dependencies:

```typescript
const quoteModule: IModule<QuoteState> = {
  initialState: quoteInitialState,
  reducers: {
    quote: {
      fn(
        state: QuoteState,
        action: QuoteAction,
        OldState: QuoteState,
        newState: QuoteState & CounterState
      ) {
        switch (action.type) {
          case "CHANGE_QUOTE":
            return action.quote;
          case "DELETE_QUOTE":
            return null;
        }
        if (newState.counter > 10) {
          return "Wow, we reached a big number";
        }
        return state;
      },
      dependencies: ["counter"],
    },
  },
};
```

We added a dependency on the counter reducer, and that will then be
run before our quote reducer. This will ensure that we can then check
that the number is bigger than 10 and react when that happens.

## Events

Modux comes with an event system that can be used when an action
happens. Events are provided in the module structure:

```typescript
const quoteModule: IModule<QuoteState> = {
  ...,
  after: {
    CHANGE_QUOTE: [
      () => {
        console.log("Run me after CHANGE_QUOTE action has been executed");
      }
    ]
  }
};

```
