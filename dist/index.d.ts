import { AnyAction, Dispatch, Store } from "redux";
export declare type Listener = (store: Store) => any;
export declare type InitFn = (store: Store) => any;
export declare type AfterHook = (dispatch: Dispatch, action: AnyAction, getState: () => IState) => any;
export interface IAfterHooks {
    [key: string]: AfterHook[];
}
export interface IState {
    [key: string]: any;
}
export interface IReducer {
    dependencies?: string[];
    fn: (state: any, action: any, ...args: any[]) => any;
}
export interface IReducers {
    [key: string]: IReducer;
}
declare type reducersFn = () => Promise<IReducers>;
export interface IModule<InitialState> {
    reducers?: IReducers;
    loadReducers?: reducersFn;
    listeners?: Listener[];
    after?: IAfterHooks;
    initialState: InitialState;
    init?: InitFn;
    name: string;
    onModuleLoad?: (module: string, store: Store) => any;
}
export default class Modux<StateType> {
    private modules;
    private store;
    private reducers;
    private initialState;
    constructor(modules: IModule<any>[] | undefined, initialState: StateType);
    addModule(module: IModule<any>): Promise<void>;
    private dispatchModuleLoaded;
    getModules(): IModule<any>[];
    private initReducers;
    createStore(): Promise<Store<StateType & {}, import("redux").Action<any>> & {
        dispatch: {};
    }>;
    getStore(): Store<StateType, AnyAction> | null;
    /**
     * Execute an individual reducer.
     * Calls dependencies first if necessary.
     */
    private executeReducer;
    private execReducers;
    private afterMiddleware;
    private afterHooks;
    private listeners;
}
export {};
