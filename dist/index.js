"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var redux_thunk_1 = require("redux-thunk");
var Modux = /** @class */ (function () {
    function Modux(modules, initialState) {
        var _this = this;
        if (modules === void 0) { modules = []; }
        this.reducers = {};
        this.afterMiddleware = function (_a) {
            var getState = _a.getState;
            return function (next) { return function (action) {
                var returnValue = next(action);
                if (action.type) {
                    _this.afterHooks(action.type.toString()).forEach(function (hook) {
                        hook(next, action, getState);
                    });
                }
                // This will likely be the action itself, unless
                // a middleware further in chain changed it.
                return returnValue;
            }; };
        };
        this.modules = modules;
        this.store = null;
        this.initialState = initialState;
    }
    Modux.prototype.addModule = function (module) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.modules.findIndex(function (m) { return m.name === module.name; }) === -1)) return [3 /*break*/, 2];
                        this.modules.push(module);
                        return [4 /*yield*/, this.initReducers()];
                    case 1:
                        _a.sent();
                        this.dispatchModuleLoaded(module);
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    Modux.prototype.dispatchModuleLoaded = function (newModule) {
        var _this = this;
        this.modules.forEach(function (module) {
            if (module.onModuleLoad && _this.store) {
                module.onModuleLoad(newModule.name, _this.store);
            }
        });
    };
    Modux.prototype.getModules = function () {
        return this.modules.slice();
    };
    Modux.prototype.initReducers = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _i, _a, module_1, loadedModules;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.reducers = {};
                        _i = 0, _a = this.modules;
                        _b.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 4];
                        module_1 = _a[_i];
                        if (module_1.reducers) {
                            this.reducers = __assign({}, this.reducers, module_1.reducers);
                        }
                        if (!module_1.loadReducers) return [3 /*break*/, 3];
                        return [4 /*yield*/, module_1.loadReducers()];
                    case 2:
                        loadedModules = _b.sent();
                        this.reducers = __assign({}, this.reducers, loadedModules);
                        _b.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Modux.prototype.createStore = function () {
        return __awaiter(this, void 0, void 0, function () {
            var store;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.initReducers()];
                    case 1:
                        _a.sent();
                        store = redux_1.createStore(function (state, action) {
                            if (state === void 0) { state = _this.initialState; }
                            return _this.execReducers(_this.reducers, state, action);
                        }, redux_1.applyMiddleware(this.afterMiddleware, redux_thunk_1.default));
                        store.subscribe(function () {
                            _this.listeners().forEach(function (fn) { return fn(store); });
                        });
                        this.store = store;
                        this.modules.forEach(function (module) {
                            if (module.init) {
                                module.init(store);
                            }
                            _this.dispatchModuleLoaded(module);
                        });
                        return [2 /*return*/, store];
                }
            });
        });
    };
    Modux.prototype.getStore = function () {
        return this.store;
    };
    /**
     * Execute an individual reducer.
     * Calls dependencies first if necessary.
     */
    Modux.prototype.executeReducer = function (name, action, reducer, reducers, oldState, newState, run) {
        if (run[name]) {
            return newState;
        }
        if (reducer.dependencies) {
            for (var _i = 0, _a = reducer.dependencies; _i < _a.length; _i++) {
                var dep = _a[_i];
                if (!reducers[dep]) {
                    throw new Error("Missing dependency: " + dep);
                }
                newState = this.executeReducer(dep, action, reducers[dep], reducers, oldState, newState, run);
                run[dep] = true;
            }
        }
        newState[name] = reducer.fn(oldState[name], action, oldState, newState);
        return newState;
    };
    Modux.prototype.execReducers = function (reducers, oldState, action) {
        var newState = __assign({}, this.initialState);
        var run = {};
        for (var name_1 in reducers) {
            if (reducers.hasOwnProperty(name_1)) {
                var reducer = reducers[name_1];
                newState = this.executeReducer(name_1, action, reducer, reducers, oldState, newState, run);
                run[name_1] = true;
            }
        }
        return newState;
    };
    Modux.prototype.afterHooks = function (name) {
        var hooks = [];
        this.modules.forEach(function (module) {
            if (module.after && module.after[name]) {
                hooks = hooks.concat(module.after[name]);
            }
        });
        return hooks;
    };
    Modux.prototype.listeners = function () {
        var listeners = [];
        this.modules.forEach(function (module) {
            if (module.listeners) {
                listeners = listeners.concat(module.listeners);
            }
        });
        return listeners;
    };
    return Modux;
}());
exports.default = Modux;
