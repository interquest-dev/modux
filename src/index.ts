import {
  AnyAction,
  applyMiddleware,
  createStore,
  Dispatch,
  Middleware,
  MiddlewareAPI,
  Store
} from "redux";
import thunk from "redux-thunk";

// Listeners are executed when the store changes (Low-level API).
export type Listener = (store: Store) => any;

// Init functions are called when the store is created.
export type InitFn = (store: Store) => any;

// After hooks are executed after a certain action has been executed.
export type AfterHook = (
  dispatch: Dispatch,
  action: AnyAction,
  getState: () => IState
) => any;

export interface IAfterHooks {
  [key: string]: AfterHook[];
}

export interface IState {
  [key: string]: any;
}

// A reducer is a normal reducer function together with an optional declaration of
// dependencies.
export interface IReducer {
  dependencies?: string[];
  fn: (state: any, action: any, ...args: any[]) => any;
}

// A definition of a map of reduicers.
export interface IReducers {
  [key: string]: IReducer;
}

type reducersFn = () => Promise<IReducers>;

// A definition of a module.
export interface IModule<InitialState> {
  reducers?: IReducers;
  loadReducers?: reducersFn;
  listeners?: Listener[];
  after?: IAfterHooks;
  initialState: InitialState;
  init?: InitFn;
  name: string;
  onModuleLoad?: (module: string, store: Store) => any;
}

export default class Modux<StateType> {
  private modules: Array<IModule<any>>;
  private store: Store<StateType> | null;
  private reducers: IReducers = {};
  private initialState: StateType;

  constructor(modules: Array<IModule<any>> = [], initialState: StateType) {
    this.modules = modules;
    this.store = null;
    this.initialState = initialState;
  }

  public async addModule(module: IModule<any>) {
    if (this.modules.findIndex(m => m.name === module.name) === -1) {
      this.modules.push(module);
      await this.initReducers();
      this.dispatchModuleLoaded(module);
    }
  }

  private dispatchModuleLoaded(newModule: IModule<unknown>) {
    this.modules.forEach(module => {
      if (module.onModuleLoad && this.store) {
        module.onModuleLoad(newModule.name, this.store);
      }
    });
  }

  public getModules() {
    return [...this.modules];
  }

  private async initReducers() {
    this.reducers = {};
    for (const module of this.modules) {
      if (module.reducers) {
        this.reducers = { ...this.reducers, ...module.reducers };
      }
      if (module.loadReducers) {
        const loadedModules = await module.loadReducers();
        this.reducers = { ...this.reducers, ...loadedModules };
      }
    }
  }

  public async createStore() {
    await this.initReducers();
    const store = createStore(
      (state: StateType = this.initialState, action) => {
        return this.execReducers(this.reducers, state, action);
      },
      applyMiddleware(this.afterMiddleware, thunk)
    );

    store.subscribe(() => {
      this.listeners().forEach(fn => fn(store));
    });
    this.store = store;
    this.modules.forEach(module => {
      if (module.init) {
        module.init(store);
      }
      this.dispatchModuleLoaded(module);
    });
    return store;
  }

  public getStore() {
    return this.store;
  }

  /**
   * Execute an individual reducer.
   * Calls dependencies first if necessary.
   */
  private executeReducer(
    name: string,
    action: any,
    reducer: IReducer,
    reducers: IReducers,
    oldState: StateType,
    newState: StateType,
    run: { [key: string]: boolean }
  ) {
    if (run[name]) {
      return newState;
    }
    if (reducer.dependencies) {
      for (const dep of reducer.dependencies) {
        if (!reducers[dep]) {
          throw new Error("Missing dependency: " + dep);
        }

        newState = this.executeReducer(
          dep,
          action,
          reducers[dep],
          reducers,
          oldState,
          newState,
          run
        );
        run[dep] = true;
      }
    }
    newState[name] = reducer.fn(oldState[name], action, oldState, newState);
    return newState;
  }

  private execReducers(reducers: IReducers, oldState: StateType, action: any) {
    let newState: StateType = { ...this.initialState };
    const run = {};
    for (const name in reducers) {
      if (reducers.hasOwnProperty(name)) {
        const reducer = reducers[name];
        newState = this.executeReducer(
          name,
          action,
          reducer,
          reducers,
          oldState,
          newState,
          run
        );
        run[name] = true;
      }
    }
    return newState;
  }

  private afterMiddleware: Middleware = ({ getState }: MiddlewareAPI) => (
    next: Dispatch
  ) => action => {
    const returnValue = next(action);
    if (action.type) {
      this.afterHooks(action.type.toString()).forEach(hook => {
        hook(next, action, getState);
      });
    }
    // This will likely be the action itself, unless
    // a middleware further in chain changed it.
    return returnValue;
  };

  private afterHooks(name: string) {
    let hooks: AfterHook[] = [];
    this.modules.forEach(module => {
      if (module.after && module.after[name]) {
        hooks = [...hooks, ...module.after[name]];
      }
    });
    return hooks;
  }

  private listeners() {
    let listeners: Listener[] = [];
    this.modules.forEach(module => {
      if (module.listeners) {
        listeners = [...listeners, ...module.listeners];
      }
    });
    return listeners;
  }
}
